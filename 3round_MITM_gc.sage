import sage.all
import random

B = 15
S = 5

real_maj = []
#set up the vars
vars = ''
for i in range(B):
    vars+= 'k%d,'%i
vars = vars[:-1]


def MAJ(a,b,c):
    return a*b+b*c+a*c 

#define unit vectors ei for k1 k21 k22
e_k1 = []
e_k21 = []
e_k22 = []

for i in range(B//3):
    e_k1.append([0 for j in range(i)] + [1] + [0 for j in range(B//3-i-1)] + [0 for j in range(2*B//3)])
    e_k21.append( [0 for j in range(B//3)]+ [0 for j in range(i)] + [1] + [0 for j in range(B//3-i-1)] + [0 for j in range(B//3)])
    e_k22.append([0 for j in range(2*B//3)]+[0 for j in range(i)] + [1] + [0 for j in range(B//3-i-1)])



#b1 and b2 functions 
def graycode(a,n):
    b = a^^(a>>1)
    b = ("{0:0{1}b}".format(b,n))
    return [ord(b[i]) - ord('0') for i in range(n)][::-1]




def b1(a):
    b = a
    for i in range(10):
        if b%2 != 0:
            return i
        else:
            b = b/2
    return -1

def b2(a):
    b = a
    b1b = b1(a)
    b = b - 2^(b1b)
    return (b1(b))


#random plaintext
pt = [0 for i in range(B)]

#random Key
key = [random.choice([0,1]) for i in range(B)]

#random key update matrix
KUM1 = random_matrix(GF(2) , B, B)
while KUM1.determinant() ==0 :
    KUM1 = random_matrix(GF(2) , B, B)

KUM2 = random_matrix(GF(2) , B, B)
while KUM2.determinant() ==0 :
    KUM2 = random_matrix(GF(2) , B, B)


KUM3 = random_matrix(GF(2) , B, B)
while KUM3.determinant() ==0 :
    KUM3 = random_matrix(GF(2) , B, B)




#random round affine matrix
A1 = random_matrix(GF(2) , B , B)
while A1.determinant() ==0:
    A1 = random_matrix(GF(2) , B, B)

A2 = random_matrix(GF(2) , B , B)
while A2.determinant() ==0:
    A2 = random_matrix(GF(2) , B, B)

A3 = random_matrix(GF(2) , B , B)
while A3.determinant() ==0:
    A3 = random_matrix(GF(2) , B, B)



def sbox(a,b,c):
    return (a+b*c , a+b+a*c , a+b+c+a*b)

def invsbox(a,b,c):
    return (a+b+b*c , b+a*c , a+b+c+a*b)




after_second_round = []

def encryption(pt,key ):
    #initial key addition
    global real_maj
    global A1
    global KUM1
    global A2
    global KUM2
    global B
    global after_second_round
    state = pt[:]
    round_key = key[:]

    for i in range(B):
        state[i] += round_key[i]

    #sbox
    for i in range(B//3):
        s = sbox(state[3*i] , state[3*i+1] , state[3*i + 2])
        state[3*i] = s[0]
        state[3*i +1 ] = s[1]
        state[3*i + 2] = s[2]
    
    #affine
    state = (A1*vector(GF(2) , state)).list()

    #key update and addition
    round_key = (KUM1 * vector(GF(2) , key)).list()
    for i in range(B):
        state[i] += round_key[i]


    #sbox
    for i in range(B//3):
        s = sbox(state[3*i] , state[3*i+1] , state[3*i + 2])
        state[3*i] = s[0]
        state[3*i +1 ] = s[1]
        state[3*i + 2] = s[2]
        real_maj.append(MAJ(s[0],s[1],s[2]))
    # print('after second sbox= ',state)
    
    #affine
    state = (A2*vector(GF(2) , state)).list()

    #key update and addition
    round_key = (KUM2 * vector(GF(2) , key)).list()
    for i in range(B):
        state[i] += round_key[i]

    after_second_round = state 
    #sbox
    for i in range(B//3):
        s = sbox(state[3*i] , state[3*i+1] , state[3*i + 2])
        state[3*i] = s[0]
        state[3*i +1 ] = s[1]
        state[3*i + 2] = s[2]
        real_maj.append(MAJ(s[0],s[1],s[2]))
    # print('after second sbox= ',state)
    
    #affine
    state = (A3*vector(GF(2) , state)).list()

    #key update and addition
    round_key = (KUM3 * vector(GF(2) , key)).list()
    for i in range(B):
        state[i] += round_key[i]


    return state

et = cputime()
ct = encryption(pt,key)
enctime = cputime(et)
# print(ct)

#forward dirrection equations
R = PolynomialRing(GF(2),vars)
# print(R)
K1 = [R('k%d'%i) for i in range(B//3)]+ [0 for i in range(2*B//3)]
real_k1 = list(vector(R,K1)(key))
K21 = [0 for i in range(B//3)] + [R('k%d'%i) for i in range(B//3,2*B//3)] + [0 for i in range(B//3)]
real_k21 = list(vector(R,K21)(key))
K22 = [0 for i in range(2*B//3)] + [R('k%d'%i) for i in range(2*B//3,B)]
real_k22 = list(vector(R,K22)(key))
FW1 = K1[:]
FW21 = K21[:]
FW22 = K22[:]

for i in range(B//3):
    s = sbox(FW1[3*i] , FW1[3*i+1] , FW1[3*i + 2])
    FW1[3*i] = s[0]
    FW1[3*i +1 ] = s[1]
    FW1[3*i + 2] = s[2]
FW1 = A1 * vector(R,FW1)
FW1 += (KUM1* vector(R,K1))


for i in range(B//3):
    s = sbox(FW21[3*i] , FW21[3*i+1] , FW21[3*i + 2])
    FW21[3*i] = s[0]
    FW21[3*i +1 ] = s[1]
    FW21[3*i + 2] = s[2]
FW21 = A1 * vector(R,FW21)
FW21 += (KUM1* vector(R,K21))


for i in range(B//3):
    s = sbox(FW22[3*i] , FW22[3*i+1] , FW22[3*i + 2])
    FW22[3*i] = s[0]
    FW22[3*i +1 ] = s[1]
    FW22[3*i + 2] = s[2]
FW22 = A1 * vector(R,FW22)
FW22 += (KUM1* vector(R,K22))

#compute D and C for FW1 FW21 FW22
zero = [0 for i in range(B)]
c1 = []
for i in range(B//3):
    c1.append(FW1(e_k1[i]) + FW1(zero))

#compute di[j]
D1 = [[0 for j in range(B//3)] for i in range(B//3)]
for i in range(B//3):
    for j in range(B//3):
        D1[i][j] = FW1(list(vector(GF(2), e_k1[i]) + vector(GF(2),e_k1[j]))) + FW1(e_k1[i]) + FW1(e_k1[j]) + FW1(zero)

c21 = []
for i in range(B//3):
    c21.append(FW21(e_k21[i]) + FW21(zero))

#compute di[j]
D21 = [[0 for j in range(B//3)] for i in range(B//3)]
for i in range(B//3):
    for j in range(B//3):
        D21[i][j] = FW21(list(vector(GF(2), e_k21[i]) + vector(GF(2),e_k21[j]))) + FW21(e_k21[i]) + FW21(e_k21[j]) + FW21(zero)

c22 = []
for i in range(B//3):
    c22.append(FW22(e_k22[i]) + FW22(zero))

#compute di[j]
D22 = [[0 for j in range(B//3)] for i in range(B//3)]
for i in range(B//3):
    for j in range(B//3):
        D22[i][j] = FW22(list(vector(GF(2), e_k22[i]) + vector(GF(2),e_k22[j]))) + FW22(e_k22[i]) + FW22(e_k22[j]) + FW22(zero)




#everything good till here

#order the monomials for linearization
var_list = []
for i in range(B):
    var_list.append(R('k%d'%i))


for i in range(B//3):
        var_list.append(R('k%d'%(3*i)) * R('k%d'%(3*i+1)))
        var_list.append(R('k%d'%(3*i)) * R('k%d'%(3*i+2)))
        var_list.append(R('k%d'%(3*i+1)) * R('k%d'%(3*i+2)))




#guess the majorities and do the second round backward
Good_key = []
keyfound = False 

tt = cputime()
for mj in range(2^(2*S)):
    if keyfound:
        break
    maj = [0 for k in range(2*S)]
    for j in range(2*S):
        maj[j] = mj//(2^j) % 2
# for maj in [real_maj]: 
    
    midstate1 = [0 for i in range(B)]
    midstate21 = [0 for i in range(B)]
    midstate22 = [0 for i in range(B)]
    constants = ct[:]

    midstate1 = vector(R , midstate1)
    midstate21 = vector(R , midstate21)
    midstate22 = vector(R , midstate22)
    constants = vector(R , constants)
    # key addition 
    midstate1 += KUM3 * vector(R,K1) 
    midstate21 += KUM3 * vector(R,K21) 
    midstate22  += KUM3 * vector(R,K22)
    #inverse affine 
    midstate1 = (1/A3) * midstate1
    midstate21 = (1/A3) * midstate21
    midstate22 = (1/A3) * midstate22
    constants = (1/A3) * constants 

    midstate1 = list(midstate1)
    midstate21 = list(midstate21)
    midstate22 = list(midstate22)
    constants = list(constants)

    #inverse S-BOX linearization
    for j in range(S):
        #for mid1
        s1 = maj[j+S] *(midstate1[3*j+1]+ midstate1[3*j+2]) + midstate1[3*j] + midstate1[3*j+1]
        s2 = maj[j+S] *(midstate1[3*j]+ midstate1[3*j+2]) + midstate1[3*j+1]
        s3 = maj[j+S] *(midstate1[3*j+1]+ midstate1[3*j]) + midstate1[3*j] + midstate1[3*j+1]+ midstate1[3*j+2]
        midstate1[3*j] = s1
        midstate1[3*j+1] = s2
        midstate1[3*j+2] = s3
        #for mid21
        s1 = maj[j+S] *(midstate21[3*j+1]+ midstate21[3*j+2]) + midstate21[3*j] + midstate21[3*j+1]
        s2 = maj[j+S] *(midstate21[3*j]+ midstate21[3*j+2]) + midstate21[3*j+1]
        s3 = maj[j+S] *(midstate21[3*j+1]+ midstate21[3*j]) + midstate21[3*j] + midstate21[3*j+1]+ midstate21[3*j+2]
        midstate21[3*j] = s1
        midstate21[3*j+1] = s2
        midstate21[3*j+2] = s3
        #for mid22
        s1 = maj[j+S] *(midstate22[3*j+1]+ midstate22[3*j+2]) + midstate22[3*j] + midstate22[3*j+1]
        s2 = maj[j+S] *(midstate22[3*j]+ midstate22[3*j+2]) + midstate22[3*j+1]
        s3 = maj[j+S] *(midstate22[3*j+1]+ midstate22[3*j]) + midstate22[3*j] + midstate22[3*j+1]+ midstate22[3*j+2]
        midstate22[3*j] = s1
        midstate22[3*j+1] = s2
        midstate22[3*j+2] = s3
        #for constants
        s1 = maj[j+S] *(constants[3*j+1]+ constants[3*j+2]+1) + constants[3*j] + constants[3*j+1]
        s2 = maj[j+S] *(constants[3*j]+ constants[3*j+2]+1) + constants[3*j+1]
        s3 = maj[j+S] *(constants[3*j+1]+ constants[3*j]+1) + constants[3*j] + constants[3*j+1]+ constants[3*j+2]
        constants[3*j] = s1
        constants[3*j+1] = s2
        constants[3*j+2] = s3
    
    
 



    midstate1 = vector(R , midstate1)# +sbox_constants
    midstate21 = vector(R , midstate21)  #+sbox_constants
    midstate22 = vector(R , midstate22)  #+sbox_constants
    constants = vector(R , constants)    
  
    #now do the inverse second round
    # key addition 
    midstate1 += KUM2 * vector(R,K1) 
    midstate21 += KUM2 * vector(R,K21) 
    midstate22  += KUM2 * vector(R,K22)
    #inverse affine 
    midstate1 = (1/A2) * midstate1
    midstate21 = (1/A2) * midstate21
    midstate22 = (1/A2) * midstate22
    constants = (1/A2) * constants 

    midstate1 = list(midstate1)
    midstate21 = list(midstate21)
    midstate22 = list(midstate22)
    constants = list(constants)

    #inverse S-BOX linearization
    for j in range(S):
        #for mid1
        s1 = maj[j] *(midstate1[3*j+1]+ midstate1[3*j+2]) + midstate1[3*j] + midstate1[3*j+1]
        s2 = maj[j] *(midstate1[3*j]+ midstate1[3*j+2]) + midstate1[3*j+1]
        s3 = maj[j] *(midstate1[3*j+1]+ midstate1[3*j]) + midstate1[3*j] + midstate1[3*j+1]+ midstate1[3*j+2]
        midstate1[3*j] = s1
        midstate1[3*j+1] = s2
        midstate1[3*j+2] = s3
        #for mid21
        s1 = maj[j] *(midstate21[3*j+1]+ midstate21[3*j+2]) + midstate21[3*j] + midstate21[3*j+1]
        s2 = maj[j] *(midstate21[3*j]+ midstate21[3*j+2]) + midstate21[3*j+1]
        s3 = maj[j] *(midstate21[3*j+1]+ midstate21[3*j]) + midstate21[3*j] + midstate21[3*j+1]+ midstate21[3*j+2]
        midstate21[3*j] = s1
        midstate21[3*j+1] = s2
        midstate21[3*j+2] = s3
        #for mid22
        s1 = maj[j] *(midstate22[3*j+1]+ midstate22[3*j+2]) + midstate22[3*j] + midstate22[3*j+1]
        s2 = maj[j] *(midstate22[3*j]+ midstate22[3*j+2]) + midstate22[3*j+1]
        s3 = maj[j] *(midstate22[3*j+1]+ midstate22[3*j]) + midstate22[3*j] + midstate22[3*j+1]+ midstate22[3*j+2]
        midstate22[3*j] = s1
        midstate22[3*j+1] = s2
        midstate22[3*j+2] = s3
        #for midct
        s1 = maj[j] *(constants[3*j+1]+ constants[3*j+2]+1) + constants[3*j] + constants[3*j+1]
        s2 = maj[j] *(constants[3*j]+ constants[3*j+2]+1) + constants[3*j+1]
        s3 = maj[j] *(constants[3*j+1]+ constants[3*j]+1) + constants[3*j] + constants[3*j+1]+ constants[3*j+2]
        constants[3*j] = s1
        constants[3*j+1] = s2
        constants[3*j+2] = s3

    

    midstate1 = vector(R , midstate1) #+sbox_constants
    midstate21 = vector(R , midstate21) # +sbox_constants
    midstate22 = vector(R , midstate22)  # +sbox_constants
    constants = vector(R , constants)
    # we have midstate1 + midstate21 + midstate22 + constants + FW1 + FW21 + FW22 = 0 
    eq1 = midstate1+FW1 
    eq21 = midstate21 + FW21
    eq22 = midstate22 + FW22 
    eqcons = constants

    #linearize eq1 and find the parity
    LEQ1 = [[0 for cc in range(2*B)] for cc2 in range(B)]
    for cc in range(B):
        for j in range(2*B):
            LEQ1[cc][j] = eq1[cc].coefficient(var_list[j]).constant_coefficient()

    LEQ1 = Matrix(GF(2), LEQ1)
    H = Matrix(GF(2),LEQ1.left_kernel().basis())


    # 0 + H * eq21 + H*eq22 + H*constants = 0 
    Good_K2 = []
    #keep value of eq21 + consttants and eq22 for good K2s 
    eval_K21 = {}
    eval_K22 = {} 
    #itterate k21 by gray code and compute FW21 
    L1 = {}
    z = [0 for i in range(B)]
    y = FW21(zero)
    hash_key = str(H*y + H*midstate21(zero) + H* constants)
    if hash_key not in L1.keys():
        L1[hash_key] = [zero] 
    else: 
        L1[hash_key].append(zero)
    eval_K21[str(zero)] = y + midstate21(zero)
    z[0] = c21[0]
    y = y+ z[0]
    
    for u in range(1,B//3):
        curr = [0 for j in range(B//3)]+graycode(2^u-1,B//3) + [0 for j in range(B//3)]
        hash_key = str(H*y+ H*midstate21(curr) + H * constants)
        if hash_key not in L1.keys():
            L1[hash_key] = [curr] 
        else: 
            L1[hash_key].append(curr)
        eval_K21[str(curr)] = y + midstate21(curr) 
        z[u] = (D21[u][u-1] + c21[u])
        y = y+ z[u]
        for v in range(2^u -1):
            curr = [0 for j in range(B//3)] + graycode(2^u+ v,B//3) + [0 for j in range(B//3)]
            hash_key = str(H*y+ H*midstate21(curr) + H * constants) 
            if hash_key not in L1.keys():
                L1[hash_key] = [curr] 
            else: 
                L1[hash_key].append(curr)
            eval_K21[str(curr)] = y + midstate21(curr)
            k = b1(2^u + v + 1)
            l = b2(2^u + v + 1)
            z[k] = z[k] + D21[k][l]
            y = y + z[k] 


    #itterate all k22 values to find a collision
    z = [0 for i in range(B)]
    y = FW22(zero)
    hash_key = str(H*y + H*midstate22(zero))
    if(hash_key in L1.keys()):
        for ll in L1[hash_key]:
            Good_K2.append(vector(GF(2),ll))
        eval_K22[str(zero)] = y + midstate22(zero)
    z[0] = c22[0]
    y = y+ z[0]

    for u in range(1,B//3):
        curr = [0 for j in range(B//3)] + [0 for j in range(B//3)] + graycode(2^u-1,B//3)
        hash_key = str(H*y+ H*midstate22(curr))
        if hash_key in L1.keys():
            for ll in L1[hash_key]:
                Good_K2.append(vector(GF(2), curr) + vector(GF(2) , ll)) 
            eval_K22[str(curr)] = y + midstate22(curr) 
        z[u] = (D22[u][u-1] + c22[u])
        y = y+ z[u]
        for v in range(2^u -1):
            curr = [0 for j in range(B//3)] + [0 for j in range(B//3)] + graycode(2^u+ v,B//3)
            hash_key = str(H*y+ H*midstate22(curr)) 
            if hash_key in L1.keys():
                for ll in L1[hash_key]:
                    Good_K2.append(vector(GF(2), curr)+ vector(GF(2) , ll))
                eval_K22[str(curr)] = y + midstate22(curr) 
            k = b1(2^u + v + 1)
            l = b2(2^u + v + 1)
            z[k] = z[k] + D22[k][l]
            y = y + z[k] 
    
    #Now check the equation eq1 + eq2 + eq3 + const = 0 

    L2 = {}
    #itterate k1 with gray code 
    z = [0 for i in range(B)]
    y = FW1(zero)
    hash_key = str(y + midstate1(zero) + constants)
    L2[hash_key] = [zero]
    
    z[0] = c1[0]
    y = y+ z[0]

    for u in range(1,B//3):
        curr = graycode(2^u-1,B//3) + [0 for j in range(B//3)]+[0 for j in range(B//3)]
        hash_key = str(y+ midstate1(curr)+ constants)
        if hash_key not in L2.keys():
            L2[hash_key] = [curr] 
        else: 
            L2[hash_key].append(curr)
        
        z[u] = (D1[u][u-1] + c1[u])
        y = y+ z[u]
        for v in range(2^u -1):
            curr = graycode(2^u+ v,B//3) +[0 for j in range(B//3)] +  [0 for j in range(B//3)]
            hash_key = str(y+ midstate1(curr) + constants ) 
            if hash_key not in L2.keys():
                L2[hash_key] = [curr] 
            else: 
                L2[hash_key].append(curr) 
            k = b1(2^u + v + 1)
            l = b2(2^u + v + 1)
            z[k] = z[k] + D1[k][l]
            y = y + z[k] 

    for k2_sample in Good_K2:
        k21_sample = [0 for j in range(B//3)] + list(k2_sample)[B//3:2*B//3] + [0 for j in range(B//3)]
        k22_sample = [0 for j in range(B//3)] + [0 for j in range(B//3)] + list(k2_sample)[2*B//3:] 
        # if str(eq21(k21_sample) + eq22(k22_sample)) in L2.keys():
        hash_key2 = str(eval_K21[str(k21_sample)] + eval_K22[str(k22_sample)])
        if hash_key2 in L2.keys():
            for ll in L2[hash_key2]:
                key_candidate = vector(GF(2),k21_sample) + vector(GF(2),k22_sample) + vector(GF(2),ll)
                if encryption(pt , list(key_candidate)) == ct:
                    print('key=',key_candidate)
                    attacktime = cputime(tt)
                    print(RR(log(attacktime/enctime)/log(2)),file = sys.stderr)
                    keyfound = True