import sage.all
import random

B = 16
S = 1
r = (8*B)//(10*S)

#not so random plaintext
pt = [0 for i in range(B)]

#random Key
Key = [random.choice([0,1]) for i in range(B)]
# print(Key)
def sbox(a,b,c):
    return (a+b*c , a+b+a*c , a+b+c+a*b)

def invsbox(a,b,c):
    return (a+b+b*c , b+a*c , a+b+c+a*b)

def majority_FNC(a,b,c):
    return a*b+a*c+b*c

#random Key update matrices
KUM = []
for i in range(r):
    KUM1 = random_matrix(GF(2) , B, B)
    while KUM1.determinant() ==0 :
        KUM1 = random_matrix(GF(2) , B, B)
    KUM.append(KUM1)

#random round affine matrices
A = []
for i in range(r):
    A1 = random_matrix(GF(2) , B , B)
    while A1.determinant() ==0:
        A1 = random_matrix(GF(2) , B, B)
    A.append(A1)


def encryption(pt,Key ):
    #initial Key addition
    global A
    global KUM
    global B
    global S
    global r

    state = pt[:]
    round_key = Key[:]
    #whitening key
    for i in range(B):
        state[i] += round_key[i]

    #sbox
    for round in range(r):
        for i in range(S):
            s = sbox(state[3*i] , state[3*i+1] , state[3*i + 2])
            state[3*i] = s[0]
            state[3*i +1 ] = s[1]
            state[3*i + 2] = s[2]

        #affine
        state = (A[round]*vector(GF(2) , state)).list()

        #key update and addition
        round_key = (KUM[round] * vector(GF(2) , Key)).list()
        for i in range(B):
            state[i] += round_key[i]

    return state

ext = cputime()
ct = encryption(pt,Key)
enctime = cputime(ext)

#implementing pushing the key forward
a = r/3
ka = [[0 for i in range(B)] for j in range(a)]
terms = ''
for i in range(B):
    terms += 'k%d,'%i
R = PolynomialRing(GF(2),terms[:-1])
master_key = vector(R,[R('k%d'%i) for i in range(B)])
current_key = vector(R,[R('k%d'%i) for i in range(B)])

for i in range(a):
    ka[i] = vector(R, [current_key[j] for j in range(3*S)]+[0 for j in range(B - 3*S)])
    current_key = current_key + ka[i]
    current_key = A[i] * current_key
    current_key += KUM[i]*master_key




# for the backward
b = 2* r//3

kc = [[0 for i in range(B)] for j in range(b)]
backward_current = KUM[r-1]*master_key

for i in range(b-1):
    backward_current = (1/A[r-1-i])*backward_current
    kc[b-i-1] = vector(R, [backward_current[j] for j in range(3*S)]+[0 for j in range(B - 3*S)])
    backward_current += kc[b-i-1]
    backward_current += KUM[r-i-2]*master_key


backward_current = (1/A[r-b])*backward_current
kc[0] = vector(R, [backward_current[j] for j in range(3*S)]+[0 for j in range(B - 3*S)])
backward_current += kc[0]

midkey = current_key+backward_current


#testing the keys are computed correctly
st2 = pt[:]
#a first rounds
for round in range(a):
    st2 = vector(GF(2),st2)
    st2 += ka[round](Key)
    #add key
    st2 = st2.list()
    for i in range(S):
        s = sbox(st2[3*i] , st2[3*i+1] , st2[3*i + 2])
        st2[3*i] = s[0]
        st2[3*i +1 ] = s[1]
        st2[3*i + 2] = s[2]
    #SBOX
    # print('sbox done')
    st2 = vector(GF(2),st2)
    st2 = A[round]*st2
#the big key addition
st2 += midkey(Key)
# print(st2)
# # last b rounds
real_maj = []
for round in range(b):
    st2 = st2.list()
    for i in range(S):
        # SBOX
        real_maj.append(majority_FNC(st2[3*i] , st2[3*i+1] , st2[3*i + 2]))
        s = sbox(st2[3*i] , st2[3*i+1] , st2[3*i + 2])
        st2[3*i] = s[0]
        st2[3*i +1 ] = s[1]
        st2[3*i + 2] = s[2]
    #key addition
    st2 = vector(GF(2),st2)
    st2 += kc[round](Key)
    # #affine
    st2 = A[a+round]*st2
# print(st2)


kd = kc[:3*b//4]
d = r//2
kc = kc[3*b//4:]
c = r//6
kb = ka[a//2:]
b = r//6
ka = ka[:a//2]
a = r//6

terms = ''
for i in range(3*S*a):
    terms += 'ka%d,'%i
for i in range(3*S*b):
    terms += 'kb%d,'%i
for i in range(3*S*c):
    terms+= 'kc%d,'%i



for i in range(3*b*S):
    terms += 'z%d,'%i


for i in range(B):
    terms += 'x%d,'%i
for i in range(B):
    terms += 'y%d,'%i

terms = terms[:-1]

R2 = PolynomialRing(GF(2),terms)
#select a generator set with
selection = [R2('ka%d'%i) for i in range(3*S*a)] + [R2('kc%d'%i) for i in range(3*S*c)] + [R2('kb%d'%i) for i in range(B-3*S*a-3*S*c)]
MAT = []
#
for keyset in ka+kc+kb:
    for key in keyset[:3]:
        column = [0 for i in range(B)]
        for i in range(B):
            if key.coefficient(R('k%d'%i)) :
                column[i] = 1
        MAT.append(column)

MAT = MAT[:B]

#get the representation of key in ka , kb and kc
VecKey = vector(R2,selection)
mat = Matrix(GF(2),MAT)
if mat.rank() != B:
    print('try again the matrix is not full rank')


#matrix computation is correct

#
else:
    Key_Representation = 1/mat * vector(R2,VecKey)
    #get the representation of K_mid by Ki s
    mid_MAT = []
    for key in midkey:
        column = [0 for i in range(B)]
        for i in range(B):
            if key.coefficient(R('k%d'%i)) :
                column[i] = 1
        mid_MAT.append(column)
    #representation of midkey in ka ,kb and kc
    mid_mat = Matrix(GF(2), mid_MAT)
    midkey_representation = mid_mat*Key_Representation

    #get representation of Kd in ka,kb,kc
    kd_MAT = []
    for keyset in kd:
        for key in keyset[:3]:
            column = [0 for i in range(B)]
            for i in range(B):
                if key.coefficient(R('k%d'%i)) :
                    column[i] = 1
            kd_MAT.append(column)
    #representation of midkey in ka ,kb and kc
    kd_mat = Matrix(GF(2), kd_MAT)
    kd_representation = kd_mat*Key_Representation
#
#
    #compute W in terms of Zi and Xi
    after_a_state = [R2('x%d'%i) for i in range(B)]
    for round in range(b):
        after_a_state = vector(R2,[R2('z%d'%i) for i in range(3*round*S , 3*(round+1)*S)]+[after_a_state[i] for i in range(3*S,B)])
        after_a_state = A[a+round]*after_a_state

    W = after_a_state


    # guess the majority of the d rounds
    maj = [0 for i in range(d*S)]
    all_good_KAKC = []
    final_keys = []

    tt = cputime()
    # for guess in range(2^(d*S)):
    #     maj = [0 for i in range(d*S)]
    #     for j in range(d*S):
    #         maj[j] = (guess//2^j)%2
    for maj in [real_maj]:
        d_state = W[:]
        #add the whitening key
        d_state += vector(R2,midkey_representation)
        for round in range(d):
            #linearize sbox
            d_state = d_state.list()
            for j in range(S):
                s1 = maj[S*round+j] *(d_state[3*j+1]+ d_state[3*j+2]+1) + d_state[3*j]
                s2 = maj[S*round+j] *(d_state[3*j]+ d_state[3*j+2]+1) + d_state[3*j] + d_state[3*j+1]
                s3 = maj[S*round+j] *(d_state[3*j+1]+ d_state[3*j]+1) + d_state[3*j] + d_state[3*j+1]+ d_state[3*j+2]
                d_state[3*j] = s1
                d_state[3*j+1] = s2
                d_state[3*j+2] = s3
            d_state = vector(R2,d_state)
            #add the round key
            #make the round key represented in ka,kb,kc
            round_key = [0 for i in range(B)]
            for j in range(3*S):
                round_key[j] = kd_representation[3*S*round + j]

            d_state += vector(R2,round_key)

            #do the affine layer
            d_state = A[a+b+round]*d_state
        Y = d_state
        equations = []
        for j in range(B):
            equations.append(R2('y%d'%j) - Y[j])

        #Create the matrix for equatoins columns represent ka kc x kb z
        equations_matrix = []
        equation_cons = []
        kbz_matrix = []
        kb_matrix = []
        for eq in equations:
            temp = [0 for ind in range(3*B+3*S*b)]
            #for kbz matrix
            temp2 = [0 for i in range(B-3*a-3*c + 3*b)]
            #for kb matrix
            temp3 = [0 for i in range(B-3*a-3*c)]

            #ka coefficients
            for j in range(3*a):
                if eq.coefficient(R2('ka%d'%j)):
                    temp[j] = 1
            #kc coefficients
            for j in range(3*c):
                if eq.coefficient(R2('kc%d'%j)):
                    temp[3*a+j] = 1

            #for x coefficients
            for j in range(B):
                if eq.coefficient(R2('x%d'%j)):
                    temp[3*a+3*c+j] = 1

            #for y coefficients
            for j in range(B):
                if eq.coefficient(R2('y%d'%j)):
                    temp[3*a+3*c+B+j] = 1

            #for kb coefficient
            for j in range(B-3*a-3*c):
                if eq.coefficient(R2('kb%d'%j)):
                    temp[2*B+3*a+3*c+j] = 1
                    temp2[j] = 1
                    temp3[j] = 1
            #for zi coefficient
            for j in range(3*S*b):
                if eq.coefficient(R2('z%d'%j)):
                    temp2[j+B-3*a-3*c] = 1
                    temp[3*B+j] = 1

            #constants
            # temp[3*B] = eq.constant_coefficient()
            equation_cons.append(eq.constant_coefficient())
            equations_matrix.append(temp)
            kbz_matrix.append(temp2)
            kb_matrix.append(temp3)

        term_vec = []
        for j in range(3*a):
            term_vec.append(R2('ka%d'%j))

        #kc coefficients
        for j in range(3*c):
            term_vec.append(R2('kc%d'%j))


        #for x coefficients
        for j in range(B):
            term_vec.append(R2('x%d'%j))


        #for y coefficients
        for j in range(B):
            term_vec.append(R2('y%d'%j))


        #for kb coefficient
        for j in range(B-3*a-3*c):
            term_vec.append(R2('kb%d'%j))

        #for zi coefficient
        for j in range(3*S*b):
            term_vec.append(R2('z%d'%j))

        term_vec = vector(R2,term_vec)
        equations_matrix = Matrix(GF(2),equations_matrix)
        kbz_matrix = Matrix(GF(2),kbz_matrix)
        kb_matrix = Matrix(GF(2),kb_matrix)
        equation_cons = vector(GF(2),equation_cons)

        #the matrix to eliminate the terms kb , z
        kbz_kernel_matrix = Matrix(GF(2), kbz_matrix.left_kernel().basis())
        kb_kernel_matrix = Matrix(GF(2), kb_matrix.left_kernel().basis())
        updated_eq = kbz_kernel_matrix*equations_matrix*term_vec + kbz_kernel_matrix*equation_cons


        #the part only having ka and x
        eq_forward = []
        eq_backward = []
        for eq in updated_eq:
            temp_eq_fw = R2(0)
            temp_eq_bw = R2(0)
            for j  in range(3*S*a):
                if eq.coefficient(R2('ka%d'%j)):
                    temp_eq_fw += R2('ka%d'%j)

            for j  in range(B):
                if eq.coefficient(R2('x%d'%j)):
                    temp_eq_fw += R2('x%d'%j)

            for j  in range(3*S*c):
                if eq.coefficient(R2('kc%d'%j)):
                    temp_eq_bw += R2('kc%d'%j)

            for j  in range(B):
                if eq.coefficient(R2('y%d'%j)):
                    temp_eq_bw += R2('y%d'%j)

            #consider the constant as a part of the fw equations
            temp_eq_fw += eq.constant_coefficient()
            eq_forward.append(temp_eq_fw)
            eq_backward.append(temp_eq_bw)

        eq_forward = vector(R2,eq_forward)
        eq_backward = vector(R2,eq_backward)

        #now do the first MITM
        #guess the value of ka
        HT = {}
        good_kakc = []


        computed_X = {}
        for keyguess in range(2^(3*S*a)):
            kaguess = []
            for j in range(3*S*a):
                kaguess.append((keyguess//2^j)%2)

            a_round_keys = []
            for j in range(a):
                temp_key = [kaguess[3*j],kaguess[3*j+1],kaguess[3*j+2]]+ [0 for l in range(B-3)]
                a_round_keys.append(temp_key)

            register = vector(GF(2),pt)
            for round in range(a):
                #add the small key
                register += vector(GF(2),a_round_keys[round])
                #Sbox
                register = register.list()
                for l in range(S):
                    s = sbox(register[3*l] , register[3*l+1] , register[3*l + 2])
                    register[3*l] = s[0]
                    register[3*l +1 ] = s[1]
                    register[3*l + 2] = s[2]
                #Affine
                register = vector(GF(2),register)
                register = A[round]*register

            computed_X[str(kaguess)] = register
            ix = str(eq_forward(kaguess + [0 for l in range(6*b+3*c)] + register.list()+[0 for l in range(B)]))
            if ix not in HT.keys():
                HT[ix] = [kaguess]
            else:
                HT[ix].append(kaguess)


        # guess the possible values of kc
        computed_Y = {}
        for keyguess in range(2^(3*S*c)):
            kcguess = []
            for j in range(3*S*c):
                kcguess.append((keyguess//2^j)%2    )

        # for kcguess in [kc_ins.list()]:
            c_round_keys = []
            for j in range(c):
                temp_key = [kcguess[3*j],kcguess[3*j+1],kcguess[3*j+2]]+ [0 for l in range(B-3)]
                c_round_keys.append(temp_key)
            register = vector(GF(2),ct)
            for round in range(c):
                #inverse affine layer
                register = vector(GF(2),register)
                register = 1/A[r-round-1] * register
                #add the round key
                register += vector(GF(2),c_round_keys[c-round -1])
                #inverse S-BOX
                register = register.list()
                for l in range(S):
                    s = invsbox(register[3*l] , register[3*l+1] , register[3*l + 2])
                    register[3*l] = s[0]
                    register[3*l +1 ] = s[1]
                    register[3*l + 2] = s[2]

            computed_Y[str(kcguess)] = register
            HT_key = str(eq_backward([0 for l in range(3*a+3*b)] + kcguess +[0 for l in range(3*b)]+[0 for l in range(B)]+register))

            if str(eq_backward([0 for l in range(3*a+3*b)] + kcguess +[0 for l in range(3*b)]+[0 for l in range(B)]+register)) in HT.keys():
                    good_kakc.append((HT[HT_key],kcguess))

                    all_good_KAKC.append((kcguess,HT[HT_key]))

        ####################################################################################################
        ###the first MITM was tested and works correctly
        ####################################################################################################
        #now do the second MITM
        #get new equations without kb in them
        kbless_eq = kb_kernel_matrix*equations_matrix*term_vec + kb_kernel_matrix*equation_cons
        #get equations in ka,kc,x,y and other ones in z_i
        left_eq = []
        right_eq = []
        for eq in kbless_eq.list():
            templeft = R2(0)
            tempright = R2(0)

            for idx in range(3*S*b):
                if eq.coefficient(R2('z%d'%idx)):
                    tempright += R2('z%d'%idx)
            templeft = eq + tempright
            left_eq.append(templeft)
            right_eq.append(tempright)

        left_eq = vector(R2,left_eq)
        right_eq = vector(R2,right_eq)

        HT2 = {}
        #go through all possible kakc values
        kakcvalwrep = []
        for kakcval in good_kakc:
            for goodkaval in kakcval[0]:
                kakcvalwrep.append((goodkaval,kakcval[1]))

        for kakcval in kakcvalwrep:
            kaguess = kakcval[0]
            kcguess = kakcval[1]
            Xguess = computed_X[str(kaguess)]
            Yguess = computed_Y[str(kcguess)]
            #fill the hash table
            ht2key = str(left_eq(kaguess+[0 for l in range(3*S*b)] + kcguess+ [0 for l in range(3*S*b)]+ Xguess.list()+Yguess))
            if ht2key not in HT2.keys():
                HT2[ht2key] = [kakcval]
            else:
                HT2[ht2key].append(kakcval)

        #go through all possible z values
        for guess in range(2^(3*S*b)):
            z_guess = []
            for l in range(3*S*b):
                z_guess.append((guess//2^l)%2)
            # print(z_guess)
            access_key = str(right_eq([0 for l in range(3*S*a + 3*S*b + 3*S*c)] + z_guess+ [0 for l in range(2*B)]))
            if access_key in HT2.keys():
                final_keys.append((z_guess,HT2[access_key]))
                #compute kb from ka,kc,z
                #compute the input of the Sboxes from z_i s
                # current_X = computed_X[str(HT2[access_key][0])]

                # kbvals = []
                # register = current_X.list()[:]
                # for round in range(b):
                #     u = invsbox(z_guess[3*round],z_guess[3*round +1 ],z_guess[3*round +2])
                #     for sboxcount in range(3):
                #         kbvals.append(register[sboxcount] + u[sboxcount])
                #     register = [z_guess[3*round],z_guess[3*round +1],z_guess[3*round +2]]+ register[3:]
                #     register = (A[a+round]*vector(GF(2),register)).list()
                #
                # #get the mail key from ka kb kc
                # guessed_key = Key_Representation()

    #having the ka,kc,z find kb and check whether
    #have a list of form z , ka , kc
    kakczworp = []
    for value in final_keys:
        z_can = value[0]
        for value2 in value[1]:
            kakczworp.append((z_can , value2[0],value2[1]))

    for key_candidate in kakczworp:
        z_candidate = key_candidate[0]
        ka_candidate = key_candidate[1]
        kc_candidate = key_candidate[2]
        u = [0 for i in range(3*S*b)]
        b_round_register = computed_X[str(ka_candidate)].list()
        kb_candidate = [0 for i in range(3*S*b)]
        for round in range(b):
            temp_u = invsbox(z_candidate[3*round],z_candidate[3*round+1],z_candidate[3*round+2])
            u[3*round] = temp_u[0]
            u[3*round+1] = temp_u[1]
            u[3*round+2] = temp_u[2]
            kb_candidate[3*round] = u[3*round]+b_round_register[0]
            kb_candidate[3*round+1] = u[3*round+1]+b_round_register[1]
            kb_candidate[3*round+2] = u[3*round+2]+b_round_register[2]
            b_round_register = [z_candidate[i] for i in range(3*round,3*round+3)] + b_round_register[3:]
            #multiply by round matrix
            b_round_register = A[a+round]*vector(GF(2),b_round_register)
            b_round_register = b_round_register.list()

        #recover master key from ka,kb,kc
        master_key_candidate = Key_Representation(ka_candidate+kb_candidate+kc_candidate+[0 for j in range(3*S*b+2*B)]).list()
        if ct == encryption(pt,master_key_candidate):
            print('key found = ',master_key_candidate)
    attacktime = cputime(tt)
