import sage.all
import random

B = 12
S = 4

#set up the vars
vars = ''
for i in range(B):
    vars+= 'k%d,'%i
vars = vars[:-1]


#random plaintext
pt = [0 for i in range(B)]

#random Key
key = [random.choice([0,1]) for i in range(B)]

#random key update matrix
KUM1 = random_matrix(GF(2) , B, B)
while KUM1.determinant() ==0 :
    KUM1 = random_matrix(GF(2) , B, B)

KUM2 = random_matrix(GF(2) , B, B)
while KUM2.determinant() ==0 :
    KUM2 = random_matrix(GF(2) , B, B)

KUM3 = random_matrix(GF(2) , B, B)
while KUM3.determinant() ==0 :
    KUM3 = random_matrix(GF(2) , B, B)



#random round affine matrix
A1 = random_matrix(GF(2) , B , B)
while A1.determinant() ==0:
    A1 = random_matrix(GF(2) , B, B)

A2 = random_matrix(GF(2) , B , B)
while A2.determinant() ==0:
    A2 = random_matrix(GF(2) , B, B)


A3 = random_matrix(GF(2) , B , B)
while A3.determinant() ==0:
    A3 = random_matrix(GF(2) , B, B)




def sbox(a,b,c):
    return (a+b*c , a+b+a*c , a+b+c+a*b)

def invsbox(a,b,c):
    return (a+b+b*c , b+a*c , a+b+c+a*b)






def encryption(pt,key ):
    #initial key addition
    global A1
    global KUM1
    global A2
    global KUM2
    global B

    state = pt[:]
    round_key = key[:]

    for i in range(B):
        state[i] += round_key[i]

    #sbox
    for i in range(B//3):
        s = sbox(state[3*i] , state[3*i+1] , state[3*i + 2])
        state[3*i] = s[0]
        state[3*i +1 ] = s[1]
        state[3*i + 2] = s[2]

    #affine
    state = (A1*vector(GF(2) , state)).list()

    #key update and addition
    round_key = (KUM1 * vector(GF(2) , key)).list()
    for i in range(B):
        state[i] += round_key[i]

    # print('midstate = ',state)
    #sbox
    for i in range(B//3):
        s = sbox(state[3*i] , state[3*i+1] , state[3*i + 2])
        state[3*i] = s[0]
        state[3*i +1 ] = s[1]
        state[3*i + 2] = s[2]

    #affine
    state = (A2*vector(GF(2) , state)).list()

    #key update and addition
    round_key = (KUM2 * vector(GF(2) , key)).list()
    for i in range(B):
        state[i] += round_key[i]


    for i in range(B//3):
        s = sbox(state[3*i] , state[3*i+1] , state[3*i + 2])
        state[3*i] = s[0]
        state[3*i +1 ] = s[1]
        state[3*i + 2] = s[2]

    #affine
    state = (A3*vector(GF(2) , state)).list()

    #key update and addition
    round_key = (KUM3 * vector(GF(2) , key)).list()
    for i in range(B):
        state[i] += round_key[i]


    return state


ct = encryption(pt,key)
# print(ct)

#forward dirrection equations
R = PolynomialRing(GF(2),vars)
# print(R)
K1 = [R('k%d'%i) for i in range(B//3)]+ [0 for i in range(2*B//3)]
K21 = [0 for i in range(B//3)] + [R('k%d'%i) for i in range(B//3,2*B//3)] + [0 for i in range(B//3)]
K22 = [0 for i in range(2*B//3)] + [R('k%d'%i) for i in range(2*B//3,B)]

FW1 = K1[:]
FW21 = K21[:]
FW22 = K22[:]

for i in range(B//3):
    s = sbox(FW1[3*i] , FW1[3*i+1] , FW1[3*i + 2])
    FW1[3*i] = s[0]
    FW1[3*i +1 ] = s[1]
    FW1[3*i + 2] = s[2]
FW1 = A1 * vector(R,FW1)
FW1 += (KUM1* vector(R,K1))


for i in range(B//3):
    s = sbox(FW21[3*i] , FW21[3*i+1] , FW21[3*i + 2])
    FW21[3*i] = s[0]
    FW21[3*i +1 ] = s[1]
    FW21[3*i + 2] = s[2]
FW21 = A1 * vector(R,FW21)
FW21 += (KUM1* vector(R,K21))


for i in range(B//3):
    s = sbox(FW22[3*i] , FW22[3*i+1] , FW22[3*i + 2])
    FW22[3*i] = s[0]
    FW22[3*i +1 ] = s[1]
    FW22[3*i + 2] = s[2]
FW22 = A1 * vector(R,FW22)
FW22 += (KUM1* vector(R,K22))

#everything good till here

#order the monomials for linearization
var_list = []
for i in range(B):
    var_list.append(R('k%d'%i))


for i in range(B//3):
        var_list.append(R('k%d'%(3*i)) * R('k%d'%(3*i+1)))
        var_list.append(R('k%d'%(3*i)) * R('k%d'%(3*i+2)))
        var_list.append(R('k%d'%(3*i+1)) * R('k%d'%(3*i+2)))

#
#guess the majorities and do the second round backward

unknowns = ''
for i in range(B):
    unknowns += 'z%d,'%i
R2 = PolynomialRing(GF(2),unknowns[:-1])

tt = cputime()
possible_keys = []
for i in range(2^(2*S)):
# for i in [123]:
    midstate = [R2('z%d'%j) for j in range(B)]
    maj = [0 for k in range(2*S)]
    for j in range(2*S):
        maj[j] = i//(2^j) % 2
    maj1 = maj[:S]
    maj2 = maj[S:]
    #linearize the second Sbox layer with the given maj
    for j in range(S):
        s1 = maj1[j] *(midstate[3*j+1]+ midstate[3*j+2]+1) + midstate[3*j]
        s2 = maj1[j] *(midstate[3*j]+ midstate[3*j+2]+1) + midstate[3*j] + midstate[3*j+1]
        s3 = maj1[j] *(midstate[3*j+1]+ midstate[3*j]+1) + midstate[3*j] + midstate[3*j+1]+ midstate[3*j+2]
        midstate[3*j] = s1
        midstate[3*j+1] = s2
        midstate[3*j+2] = s3

    midstate = vector(R2,midstate)
    #affine layer
    midstate = A2* midstate
    # cons_vec = vector(R2,[midstate[i].constant_coefficient() for i in range(B)])
    # midstate -= cons_vec

    #keys
    K1 = vector(R,K1)
    K21 = vector(R,K21)
    K22 = vector(R,K22)

    added_key1 = KUM2 * K1
    added_key21 = KUM2 * K21
    added_key22 = KUM2 * K22
    added_key1 = list(added_key1)
    added_key21 = list(added_key21)
    added_key22 = list(added_key22)
    midstate = list(midstate)
    #now the third round
    #sbox for midstate
    for j in range(S):
        s1 = maj2[j] *(midstate[3*j+1]+ midstate[3*j+2]+1) + midstate[3*j]
        s2 = maj2[j] *(midstate[3*j]+ midstate[3*j+2]+1) + midstate[3*j] + midstate[3*j+1]
        s3 = maj2[j] *(midstate[3*j+1]+ midstate[3*j]+1) + midstate[3*j] + midstate[3*j+1]+ midstate[3*j+2]
        midstate[3*j] = s1
        midstate[3*j+1] = s2
        midstate[3*j+2] = s3

    #sbox for added key
    for j in range(S):
        s1 = maj2[j] *(added_key1[3*j+1]+ added_key1[3*j+2]+1) + added_key1[3*j]
        s2 = maj2[j] *(added_key1[3*j]+ added_key1[3*j+2]+1) + added_key1[3*j] + added_key1[3*j+1]
        s3 = maj2[j] *(added_key1[3*j+1]+ added_key1[3*j]+1) + added_key1[3*j] + added_key1[3*j+1]+ added_key1[3*j+2]
        added_key1[3*j] = s1
        added_key1[3*j+1] = s2
        added_key1[3*j+2] = s3
        s1 = maj2[j] *(added_key21[3*j+1]+ added_key21[3*j+2]+1) + added_key21[3*j]
        s2 = maj2[j] *(added_key21[3*j]+ added_key21[3*j+2]+1) + added_key21[3*j] + added_key21[3*j+1]
        s3 = maj2[j] *(added_key21[3*j+1]+ added_key21[3*j]+1) + added_key21[3*j] + added_key21[3*j+1]+ added_key21[3*j+2]
        added_key21[3*j] = s1
        added_key21[3*j+1] = s2
        added_key21[3*j+2] = s3
        s1 = maj2[j] *(added_key22[3*j+1]+ added_key22[3*j+2]+1) + added_key22[3*j]
        s2 = maj2[j] *(added_key22[3*j]+ added_key22[3*j+2]+1) + added_key22[3*j] + added_key22[3*j+1]
        s3 = maj2[j] *(added_key22[3*j+1]+ added_key22[3*j]+1) + added_key22[3*j] + added_key22[3*j+1]+ added_key22[3*j+2]
        added_key22[3*j] = s1
        added_key22[3*j+1] = s2
        added_key22[3*j+2] = s3

    #affine layer
    midstate = vector(R2,midstate)
    added_key1 = vector(R,added_key1)
    added_key21 = vector(R,added_key21)
    added_key22 = vector(R,added_key22)
    #affine layer
    midstate = A3* midstate
    added_key1 = A3* added_key1
    added_key21 = A3* added_key21
    added_key22 = A3* added_key22
    #add key 
    added_key1 += KUM3 * K1
    added_key21 += KUM3 * K21
    added_key22 += KUM3 * K22

    eq1 = midstate(FW1.list()) + added_key1
    eq21 = midstate(FW21.list()) + added_key21
    eq22 = midstate(FW22.list()) + added_key22
    
    #linearize the equations in dimension 2b
    LEQ1 = [[0 for cc in range(2*B)] for cc2 in range(B)]
    EQcons1 = [0 for cc in range(B)]
    LEQ21 = [[0 for cc in range(2*B)] for cc2 in range(B)]
    EQcons21 = [0 for cc in range(B)]
    LEQ22 = [[0 for cc in range(2*B)] for cc2 in range(B)]
    EQcons22 = [0 for cc in range(B)]
    for cc in range(B):
        for j in range(2*B):
            LEQ1[cc][j] = eq1[cc].coefficient(var_list[j]).constant_coefficient()
            EQcons1[cc] = eq1[cc].constant_coefficient()
            LEQ21[cc][j] = eq21[cc].coefficient(var_list[j]).constant_coefficient()
            EQcons21[cc] = eq21[cc].constant_coefficient()
            LEQ22[cc][j] = eq22[cc].coefficient(var_list[j]).constant_coefficient()
            EQcons22[cc] = eq22[cc].constant_coefficient()

    LEQ1 = Matrix(GF(2), LEQ1)
    LEQ21 = Matrix(GF(2), LEQ21)
    LEQ22 = Matrix(GF(2), LEQ22)
    EQconst = vector(GF(2),EQcons1) + vector(GF(2),EQcons21) + vector(GF(2),EQcons22)
    Parity1 = Matrix(GF(2),LEQ1.left_kernel().basis())
    ########################################################
    #everything is computed correctly up till here
    ########################################################
    #first MITM
    Good_K2 = []
    K21_L = {}
    #first MITM
    #try all K21 values
    for cc in range(2^(B//3)):
        current_k21 = [0 for j in range(B//3)]+[(cc//2^j)%2 for j in range(B//3)]+[0 for j in range(B//3)]
        # print(current_k21)
        #add the quadratics
        for j in range(B//3):
            current_k21.append(current_k21[3*j]*current_k21[3*j+1])
            current_k21.append(current_k21[3*j]*current_k21[3*j+2])
            current_k21.append(current_k21[3*j+1]*current_k21[3*j+2])
        # ctprime = ct[:]
        # for j in range(B//3):
        #     ctprime.append(ct[3*j]*ct[3*j+1])
        #     ctprime.append(ct[3*j]*ct[3*j+2])
        #     ctprime.append(ct[3*j+1]*ct[3*j+2])

        # EQconstprime = EQconst.list()[:]
        # for j in range(B//3):
        #     EQconstprime.append(EQconst[3*j]*EQconst[3*j+1])
        #     EQconstprime.append(EQconst[3*j]*EQconst[3*j+2])
        #     EQconstprime.append(EQconst[3*j+1]*EQconst[3*j+2])

        #multiply by the parity matrix
        hash_key = str(Parity1 * LEQ21 * vector(GF(2),current_k21)+ Parity1* vector(GF(2),EQconst)+ Parity1*vector(GF(2),ct))
        K21_L[str(hash_key)] = current_k21

    #try all K22 values
    for cc in range(2^(B//3)):
        current_k22 = [0 for j in range(2*B//3)]+[(cc//2^j)%2 for j in range(B//3)]
        # print(current_k22)
        for j in range(B//3):
            current_k22.append(current_k22[3*j]*current_k22[3*j+1])
            current_k22.append(current_k22[3*j]*current_k22[3*j+2])
            current_k22.append(current_k22[3*j+1]*current_k22[3*j+2])

        hash_key2 = str(Parity1 * LEQ22 * vector(GF(2),current_k22))
        if hash_key2 in K21_L.keys():
            Good_K2.append(vector(GF(2),current_k22[:B]) + vector(GF(2), K21_L[hash_key2][:B]) )

        ##################################################
        #The good K2 does showup in one of the lists
        ###################################################
    #now do the second meet in the middle
    #guess the value of K1
    L2 = {}
    for k2guess in Good_K2:
        k2guess = k2guess.list()
        for j in range(B//3):
            k2guess.append(k2guess[3*j]*k2guess[3*j+1])
            k2guess.append(k2guess[3*j]*k2guess[3*j+2])
            k2guess.append(k2guess[3*j+1]*k2guess[3*j+2])
        # print(k2guess )
        LEQ2 = LEQ21+LEQ22
        hashkey3 = LEQ2*vector(GF(2),k2guess)+ vector(GF(2),ct)+ EQconst
        L2[str(hashkey3)]= k2guess[:B]

    for j in range(2^(B//3)):
        k1guess = []

        for k in range(B//3):
            k1guess.append(j//2^k %2)

        for k in range(2*B//3):
            k1guess.append(0)

        for k in range(B//3):
            k1guess.append(k1guess[3*k]*k1guess[3*k+1])
            k1guess.append(k1guess[3*k]*k1guess[3*k+2])
            k1guess.append(k1guess[3*k+1]*k1guess[3*k+2])
        if str(LEQ1 * vector(GF(2),k1guess)) in L2.keys():
            # print('hit')
            possible_keys.append(vector(GF(2),k1guess[:B]) + vector(GF(2),L2[str(LEQ1 * vector(GF(2),k1guess))]))



for candidate in possible_keys:
    if encryption(pt,candidate.list()) == ct:
        print('key found',candidate)

attacktime = cputime(tt)
print('attack time = ',attacktime)

et = cputime()
ct = encryption(pt,key)
enctime = cputime(et)
print('encryption time = ',enctime)
print('complexity = 2^' , RR(log(attacktime/enctime)/log(2)))